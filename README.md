# djMixer


## Content

This repository contains the source code for a basic dj mixer.
It was developed and tested with Qt 6.3.1 in the Qt Creator 8.0.1 under macOS Monterey on a M1 Pro platform. 


## Build Process
Clone the repository into a local folder. <br />
Open your Qt Creator. <br />
Create a new Qt project from the AudioPlayer.pro file. <br />
Build the project. <br />
Run the project. <br />
