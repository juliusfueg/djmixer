#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMediaPlayer>
#include <QMessageBox>
#include <QSlider>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QFormLayout>
#include <QPushButton>
#include <QToolButton>
#include <QDialog>
#include <QAudioOutput>
#include <QAudioDevice>
#include <QMediaDevices>

QT_BEGIN_NAMESPACE
namespace Ui {
    class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:

    /* general functions */
    void exitDjMixer();
    void aboutDjMixer();
    void updateAudioDevices();
    void deviceChanged(int idx);

    /* Deck A */
    void openAudioFileDeckA();
    void playOrPauseDeckA();
    void cueDeckA();
    void stopDeckA();
    void muteUnmuteDeckA();
    void loudnessSliderDeckA_valueChanged(int value);
    void pitchSliderDeckA_valueChanged(int pitch);
    void pitchResetDeckA();
    void thirtyThreeRpmDeckA();
    void fourtyFiveRpmDeckA();
    void doublePitchRangeDeckA();

    /* Deck B */
    void openAudioFileDeckB();
    void playOrPauseDeckB();
    void cueDeckB();
    void stopDeckB();
    void muteUnmuteDeckB();
    void loudnessSliderDeckB_valueChanged(int value);
    void pitchSliderDeckB_valueChanged(int pitch);
    void pitchResetDeckB();
    void thirtyThreeRpmDeckB();
    void fourtyFiveRpmDeckB();
    void doublePitchRangeDeckB();

    /* cross fader */
    void crossFadeSlider_valueChanged(int value);

private:

    /* pointers to main objects */
    Ui::MainWindow *ui;
    QMainWindow *newWindow;
    QAudioDevice deviceInfo1DeckA;
    QAudioDevice deviceInfo2DeckA;
    QAudioDevice deviceInfo1DeckB;
    QAudioDevice deviceInfo2DeckB;

    /* Deck A */
    QMediaPlayer *audioPlayerDeckA;
    QAudioOutput* audioOutputDeckA;
    QMediaDevices* audioDevicesDeckA;
    void setTimeLabelDeckA();
    void setTimeProgressDeckA();
    void setTurntableProgressDeckA();
    void playAudioDeckA();
    void pauseAudioDeckA();
    double speedDeckA;
    int rpmDeckA;
    int pitchRangeDeckA;

    /* Deck B */
    QMediaPlayer *audioPlayerDeckB;
    QAudioOutput* audioOutputDeckB;
    QMediaDevices* audioDevicesDeckB;
    void setTimeLabelDeckB();
    void setTimeProgressDeckB();
    void setTurntableProgressDeckB();
    void playAudioDeckB();
    void pauseAudioDeckB();
    double speedDeckB;
    int rpmDeckB;
    int pitchRangeDeckB;

};
#endif // MAINWINDOW_H
