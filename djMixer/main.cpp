#include "mainwindow.h"

#include <QApplication>
#include <QWidget>

int main(int argc, char *argv[])
{
    QApplication djMixer(argc, argv);
    QApplication::setApplicationName("DjMixer");

    MainWindow w;
    w.setWindowTitle("DjMixer");
    w.setWindowIcon(QIcon(":/images/images/play.png"));
    w.setWindowState(Qt::WindowMaximized);
    w.show();

    return djMixer.exec();
}
