#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QMessageBox"
#include "QFileDialog"
#include "QTextStream"
#include "QSlider"
#include "QDebug"
#include <qmediadevices.h>
using std::to_string;
using std::string;

/* constructor */
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{





    /* set up main window */
    ui->setupUi(this);
    connect(ui->exitDjMixer, &QAction::triggered, this, &MainWindow::exitDjMixer);
    connect(ui->aboutDjMixer, &QAction::triggered, this, &MainWindow::aboutDjMixer);





    /* ##### DECK A ##### */
    /* create new objects */
    audioPlayerDeckA = new QMediaPlayer(this);
    audioOutputDeckA = new QAudioOutput(this);
    audioDevicesDeckA = new QMediaDevices(this);

    /* set audio output to the audio player object */
    audioPlayerDeckA->setAudioOutput(audioOutputDeckA);

    /* open audio file */
    connect(ui->openAudioFileBtnDeckA, &QToolButton::clicked, this, &MainWindow::openAudioFileDeckA);
    ui->openAudioFileBtnDeckA->setText("Open File");

    /* play, pause, stop */
    connect(ui->playPauseBtnDeckA, &QToolButton::clicked, this, &MainWindow::playOrPauseDeckA);
    connect(ui->stopBtnDeckA, &QToolButton::clicked, this, &MainWindow::stopDeckA);
    ui->stopBtnDeckA->setIcon(QIcon(":/images/images/stop.png"));

    /* cue */
    connect(ui->cueBtnDeckA, &QToolButton::clicked, this, &MainWindow::cueDeckA);
    connect(ui->outputDevice1BoxDeckA, QOverload<int>::of(&QComboBox::activated), this, &MainWindow::deviceChanged);
    connect(ui->outputDevice2BoxDeckA, QOverload<int>::of(&QComboBox::activated), this, &MainWindow::deviceChanged);
    connect(audioDevicesDeckA, &QMediaDevices::audioOutputsChanged, this, &MainWindow::updateAudioDevices);
    updateAudioDevices();
    ui->outputDevice1BoxDeckA->setCurrentIndex(0);
    ui->outputDevice2BoxDeckA->setCurrentIndex(0);
    deviceChanged(0);
    ui->cueBtnDeckA->setIcon(QIcon(":/images/images/headphone_output.png"));
    //connect(ui->outputDevice1BoxDeckA, &QComboBox::currentIndexChanged, this, &MainWindow::setAudioOutput);

    /* pitch functionality */
    connect(ui->pitchSliderDeckA, &QSlider::sliderMoved, this, &MainWindow::pitchSliderDeckA_valueChanged);
    connect(ui->doublePitchDeckA, &QToolButton::clicked, this, &MainWindow::doublePitchRangeDeckA);
    pitchRangeDeckA = 16; /* set to 16% pitch range here so that it will be set to 8% pitch range in function */
    doublePitchRangeDeckA();
    ui->doublePitchDeckA->setText("2x");
    this->pitchSliderDeckA_valueChanged(1000);
    ui->pitchSliderDeckA->setTickPosition(QSlider::TicksAbove);
    connect(ui->pitchResetButtonDeckA, &QToolButton::clicked, this, &MainWindow::pitchResetDeckA);
    ui->pitchResetButtonDeckA->setText("Reset");

    /* adjust loudness */
    connect(ui->loudnessSliderDeckA, &QSlider::sliderMoved, this, &MainWindow::loudnessSliderDeckA_valueChanged);
    connect(ui->muteUnmuteBtnDeckA, &QToolButton::clicked, this, &MainWindow::muteUnmuteDeckA);
    ui->loudnessSliderDeckA->setRange(0, 100);
    this->loudnessSliderDeckA_valueChanged(50); /* set the initial volume to 50 */

    /* connect position and timer of audio player */
    connect(audioPlayerDeckA, &QMediaPlayer::durationChanged, ui->timeSliderDeckA, &QSlider::setMaximum);
    connect(audioPlayerDeckA, &QMediaPlayer::positionChanged, ui->timeSliderDeckA, &QSlider::setValue);
    connect(ui->timeSliderDeckA, &QSlider::sliderMoved, audioPlayerDeckA, &QMediaPlayer::setPosition); /* valueChanged for listenting to the steps */
    connect(audioPlayerDeckA, &QMediaPlayer::positionChanged, this, &MainWindow::setTimeProgressDeckA);

    /* rpm functionality */
    connect(ui->thirtyThreeRpmButtonDeckA, &QToolButton::clicked, this, &MainWindow::thirtyThreeRpmDeckA);
    connect(ui->fourtyFiveRpmButtonDeckA, &QToolButton::clicked, this, &MainWindow::fourtyFiveRpmDeckA);
    thirtyThreeRpmDeckA();
    ui->turntableDeckA->setWrapping(1); /* remove gap from dial */
    connect(ui->turntableDeckA, &QDial::valueChanged, this, &MainWindow::setTurntableProgressDeckA);





    /* ##### DECK B ##### */
    /* create new objects */
    audioPlayerDeckB = new QMediaPlayer(this);
    audioOutputDeckB = new QAudioOutput(this);
    audioDevicesDeckB = new QMediaDevices(this);

    /* set audio output to the audio player object */
    audioPlayerDeckB->setAudioOutput(audioOutputDeckB);

    /* open audio file */
    connect(ui->openAudioFileBtnDeckB, &QToolButton::clicked, this, &MainWindow::openAudioFileDeckB);
    ui->openAudioFileBtnDeckB->setText("Open File");

    /* play, pause, stop */
    connect(ui->playPauseBtnDeckB, &QToolButton::clicked, this, &MainWindow::playOrPauseDeckB);
    connect(ui->stopBtnDeckB, &QToolButton::clicked, this, &MainWindow::stopDeckB);
    ui->stopBtnDeckB->setIcon(QIcon(":/images/images/stop.png"));

    /* cue */
    connect(ui->cueBtnDeckB, &QToolButton::clicked, this, &MainWindow::cueDeckB);
    connect(ui->outputDevice1BoxDeckB, QOverload<int>::of(&QComboBox::activated), this, &MainWindow::deviceChanged);
    connect(ui->outputDevice2BoxDeckB, QOverload<int>::of(&QComboBox::activated), this, &MainWindow::deviceChanged);
    connect(audioDevicesDeckB, &QMediaDevices::audioOutputsChanged, this, &MainWindow::updateAudioDevices);
    updateAudioDevices();
    ui->outputDevice1BoxDeckB->setCurrentIndex(0);
    ui->outputDevice2BoxDeckB->setCurrentIndex(0);
    deviceChanged(0);
    ui->cueBtnDeckB->setIcon(QIcon(":/images/images/headphone_output.png"));
    //connect(ui->outputDevice1BoxDeckB, &QComboBox::currentIndexChanged, this, &MainWindow::setAudioOutput);

    /* pitch functionality */
    connect(ui->pitchSliderDeckB, &QSlider::sliderMoved, this, &MainWindow::pitchSliderDeckB_valueChanged);
    connect(ui->doublePitchDeckB, &QToolButton::clicked, this, &MainWindow::doublePitchRangeDeckB);
    pitchRangeDeckB = 16; /* set to 16% pitch range here so that it will be set to 8% pitch range in function */
    doublePitchRangeDeckB();
    ui->doublePitchDeckB->setText("2x");
    this->pitchSliderDeckB_valueChanged(1000);
    ui->pitchSliderDeckB->setTickPosition(QSlider::TicksAbove);
    connect(ui->pitchResetButtonDeckB, &QToolButton::clicked, this, &MainWindow::pitchResetDeckB);
    ui->pitchResetButtonDeckB->setText("Reset");

    /* adjust loudness */
    connect(ui->loudnessSliderDeckB, &QSlider::sliderMoved, this, &MainWindow::loudnessSliderDeckB_valueChanged);
    connect(ui->muteUnmuteBtnDeckB, &QToolButton::clicked, this, &MainWindow::muteUnmuteDeckB);
    ui->loudnessSliderDeckB->setRange(0, 100);
    this->loudnessSliderDeckB_valueChanged(50); /* set the initial volume to 50 */

    /* connect position and timer of audio player */
    connect(audioPlayerDeckB, &QMediaPlayer::durationChanged, ui->timeSliderDeckB, &QSlider::setMaximum);
    connect(audioPlayerDeckB, &QMediaPlayer::positionChanged, ui->timeSliderDeckB, &QSlider::setValue);
    connect(ui->timeSliderDeckB, &QSlider::sliderMoved, audioPlayerDeckB, &QMediaPlayer::setPosition); /* valueChanged for listenting to the steps */
    connect(audioPlayerDeckB, &QMediaPlayer::positionChanged, this, &MainWindow::setTimeProgressDeckB);

    /* rpm functionality */
    connect(ui->thirtyThreeRpmButtonDeckB, &QToolButton::clicked, this, &MainWindow::thirtyThreeRpmDeckB);
    connect(ui->fourtyFiveRpmButtonDeckB, &QToolButton::clicked, this, &MainWindow::fourtyFiveRpmDeckB);
    thirtyThreeRpmDeckB();
    ui->turntableDeckB->setWrapping(1); /* remove gap from dial */
    connect(ui->turntableDeckB, &QDial::valueChanged, this, &MainWindow::setTurntableProgressDeckB);






    /* ##### CROSS FADER ##### */
    ui->crossFadeSlider->setRange(0, 100);
    this->crossFadeSlider_valueChanged(50);

}

/* destructor */
MainWindow::~MainWindow()
{
    delete ui;
    delete audioPlayerDeckA;
    delete audioOutputDeckA;
    delete audioDevicesDeckA;
    delete audioPlayerDeckB;
    delete audioOutputDeckB;
    delete audioDevicesDeckB;
}

/* ##### file management ##### */
/* window with app info */
void MainWindow::aboutDjMixer()
{
    QMessageBox::about(this, "DJ Mixer", "This is a simple DJ Mixer made with Qt 6. "
                                         "The application has two decks which each can perform functions such as play, pause, stop, "
                                         "change of playback speed, and loudness adjustment. "
                                         "The DJ Mixer was tested to support .wav, .aif and .mp3 formats."
                                         "Additionally there is a cross fader to fade the volume between the two decks.");
}

/* close the app */
void MainWindow::exitDjMixer()
{
    QApplication::exit(0);
}





/* ##### DECK A ##### */
/* open audio file */
void MainWindow::openAudioFileDeckA()
{
    // open the audio file and get the pathname
    //QString name = QFileDialog::getOpenFileName(this, "Open Audio", QDir::homePath(), "*.mpg;*.mp4");

    // if the above format doesn't work the below must work on both linux and mac
    QString name = QFileDialog::getOpenFileName(this, "Open Audio", QDir::homePath(), "Audio File (*.*)");

    // get the file name from the file information
    QFile f(name);
    QFileInfo fileInfo(f.fileName());
    QString fileName = fileInfo.fileName();

    // set the window to file name
    setWindowTitle(fileName);
    ui->statusbar->showMessage("Playing " + fileName);

    // set the time label
    connect(audioPlayerDeckA, &QMediaPlayer::durationChanged, this, [&]() {
        setTimeLabelDeckA();
    });

    // add the path of the audio file to the QMediaPlayer and play
    stopDeckA();
    audioPlayerDeckA->setSource(QUrl::fromLocalFile(name));
    playAudioDeckA();
}


/* ##### audio playback ##### */
/* play and pause */
void MainWindow::playOrPauseDeckA()
{
    // get the current state of the audio player
    QMediaPlayer::PlaybackState stt = audioPlayerDeckA->playbackState();

    // if the state is pause then play and the button is clicked then play. If not in paused state pause the audio.
    if (stt == audioPlayerDeckA->PausedState || stt == audioPlayerDeckA->StoppedState) {
        playAudioDeckA();
    } else {
        pauseAudioDeckA();
    }
}

/* cue */
void MainWindow::cueDeckA()
{

    int indexBox1 = ui->outputDevice1BoxDeckA->currentIndex();
    int indexBox2 = ui->outputDevice2BoxDeckA->currentIndex();


    if(indexBox1 == indexBox2){

    }


    if(audioOutputDeckA->device() == audioDevicesDeckA->audioOutputs().at(indexBox1))
    {
        audioOutputDeckA->setDevice(audioDevicesDeckA->audioOutputs().at(indexBox2));
        ui->cueBtnDeckA->setIcon(QIcon(":/images/images/speaker_output.png"));
    }
    else
    {
        audioOutputDeckA->setDevice(audioDevicesDeckA->audioOutputs().at(indexBox1));
        ui->cueBtnDeckA->setIcon(QIcon(":/images/images/headphone_output.png"));
    }
}

/* stop */
void MainWindow::stopDeckA()
{
    audioPlayerDeckA->stop();
    ui->playPauseBtnDeckA->setIcon(QIcon(":/images/images/play.png"));
}

/* pitch */
void MainWindow::pitchSliderDeckA_valueChanged(int pitch)
{
    audioPlayerDeckA->setPlaybackRate(float(pitch)/1000);
    ui->pitchSliderDeckA->setSliderPosition(pitch);
    ui->speedLblDeckA->setText(QString::number( (float(pitch) / 1000 - 1.0) * 100) );
}

void MainWindow::pitchResetDeckA(){
    audioPlayerDeckA->setPlaybackRate(1.0);
    ui->pitchSliderDeckA->setSliderPosition(1000);
    ui->speedLblDeckA->setText(QString::number(0.0));
}

/* loudness button */
void MainWindow::muteUnmuteDeckA()
{
    // change the slider position and the volume if the loudness button is clicked
    if (ui->loudnessSliderDeckA->value() == 0.0) {
        audioOutputDeckA->setVolume(0.2);
        ui->loudnessSliderDeckA->setSliderPosition(20);
    } else {
        audioOutputDeckA->setVolume(0.0);
        ui->loudnessSliderDeckA->setSliderPosition(0);
    }
}

/* loudness slider */
void MainWindow::loudnessSliderDeckA_valueChanged(int value)
{
    // set the volume of the audio player. If the volume is 0 icon changes to mute and change the slide position and vice versa.
    audioOutputDeckA->setVolume(float(value)/100);

    if(value == 0) {
        ui->muteUnmuteBtnDeckA->setIcon(QIcon(":/images/images/mute.png"));
    } else {
        ui->muteUnmuteBtnDeckA->setIcon(QIcon(":/images/images/sound.png"));
    }

    ui->loudnessSliderDeckA->setSliderPosition(value);
}

/* set length of audio as label */
void MainWindow::setTimeLabelDeckA()
{
    // convert duration into HH:MM:SS format and set the label
    string timeLbl;
    long milli = audioPlayerDeckA->duration();
    long hr = milli / 3600000;
    milli = milli - 3600000 * hr;
    long min = milli / 60000;
    milli = milli - 60000 * min;
    long sec = milli / 1000;

    if (hr > 9) {
        timeLbl = to_string(hr) + ":";
    } else {
        timeLbl = "0" + to_string(hr) + ":";
    }
    if(min > 9){
        timeLbl += to_string(min) + ":";
    }else{
        timeLbl += "0" + to_string(min) + ":";
    }
    if(sec > 9){
        timeLbl += to_string(sec);
    }else{
        timeLbl += "0" + to_string(sec);
    }

    ui->absoluteTimeLabelDeckA->setText(QString::fromStdString(timeLbl));

}

/* set time progress */
void MainWindow::setTimeProgressDeckA()
{
    // convert duration into HH:MM:SS format and set the label
    string timeLbl;
    long milli = audioPlayerDeckA->position();
    long hr = milli / 3600000;
    milli = milli - 3600000 * hr;
    long min = milli / 60000;
    milli = milli - 60000 * min;
    long sec = milli / 1000;

    if (hr > 9) {
        timeLbl = to_string(hr) + ":";
    } else {
        timeLbl = "0" + to_string(hr) + ":";
    }
    if(min > 9){
        timeLbl += to_string(min) + ":";
    }else{
        timeLbl += "0" + to_string(min) + ":";
    }
    if(sec > 9){
        timeLbl += to_string(sec);
    }else{
        timeLbl += "0" + to_string(sec);
    }

    ui->timeProgressLabelDeckA->setText(QString::fromStdString(timeLbl));

    /* set position of turn table */
    int pos = 0;
    if(rpmDeckA == 45){
        pos = milli % 1333; // 60000 ms / 45 rpm = 1333,33 ms per rotation
    }
    else if(rpmDeckA == 33)
    {
        pos = milli % 1818; // 60000 ms / 33 rpm = 1818,18 ms per rotation
    }
    ui->turntableDeckA->setSliderPosition(pos);

}

void MainWindow::setTurntableProgressDeckA()
{
    /*
    long posTurntable = ui->turntableDeckA->sliderPosition();
    long posAudioPlayer = audioPlayerDeckA->position();
    long diff = posTurntable - (posAudioPlayer % 1333);
    audioPlayerDeckA->setPosition(posAudioPlayer - diff);
    */
}

/* play audio file */
void MainWindow::playAudioDeckA()
{
    audioPlayerDeckA->play();
    ui->playPauseBtnDeckA->setIcon(QIcon(":/images/images/pause.png"));
}

/* pause audio file */
void MainWindow::pauseAudioDeckA()
{
    audioPlayerDeckA->pause();
    ui->playPauseBtnDeckA->setIcon(QIcon(":/images/images/play.png"));
}

void MainWindow::thirtyThreeRpmDeckA()
{
    ui->turntableDeckA->setRange(0,1818); // 60000 ms / 33 rpm = 1818,18 ms per rotation
    rpmDeckA = 33;
    ui->thirtyThreeRpmButtonDeckA->setStyleSheet("background-color: rgb(224, 195, 30)");
    ui->fourtyFiveRpmButtonDeckA->setStyleSheet("background-color: rgb(93, 91, 89)");
}

void MainWindow::fourtyFiveRpmDeckA()
{
    ui->turntableDeckA->setRange(0,1333); // 60000 ms / 45 rpm = 1333,33 ms per rotation
    rpmDeckA = 45;
    ui->fourtyFiveRpmButtonDeckA->setStyleSheet("background-color: rgb(224, 195, 30)");
    ui->thirtyThreeRpmButtonDeckA->setStyleSheet("background-color: rgb(93, 91, 89)");
}

void MainWindow::doublePitchRangeDeckA()
{
    if(pitchRangeDeckA == 8){
        ui->pitchSliderDeckA->setRange(840, 1160); // +/- 16% pitch
        pitchRangeDeckA = 16;
        ui->doublePitchDeckA->setStyleSheet("background-color: rgb(224, 195, 30)");
    }
    else if(pitchRangeDeckA == 16){
        /* clip pitch value when changing from 16% pitch to 8% pitch */
        if(ui->pitchSliderDeckA->value() < 920){
            pitchSliderDeckA_valueChanged(920);
        }
        if(ui->pitchSliderDeckA->value() > 1080){
            pitchSliderDeckA_valueChanged(1080);
        }

        ui->pitchSliderDeckA->setRange(920, 1080); // +/- 8% pitch
        pitchRangeDeckA = 8;
        ui->doublePitchDeckA->setStyleSheet("background-color: rgb(93, 91, 89)");
    }
}

























/* ##### DECK B ##### */
/* open audio file */
void MainWindow::openAudioFileDeckB()
{
    // open the audio file and get the pathname
    //QString name = QFileDialog::getOpenFileName(this, "Open Audio", QDir::homePath(), "*.mpg;*.mp4");

    // if the above format doesn't work the below must work on both linux and mac
    QString name = QFileDialog::getOpenFileName(this, "Open Audio", QDir::homePath(), "Audio File (*.*)");

    // get the file name from the file information
    QFile f(name);
    QFileInfo fileInfo(f.fileName());
    QString fileName = fileInfo.fileName();

    // set the window to file name
    setWindowTitle(fileName);
    ui->statusbar->showMessage("Playing " + fileName);

    // set the time label
    connect(audioPlayerDeckB, &QMediaPlayer::durationChanged, this, [&]() {
        setTimeLabelDeckB();
    });

    // add the path of the audio file to the QMediaPlayer and play
    stopDeckB();
    audioPlayerDeckB->setSource(QUrl::fromLocalFile(name));
    playAudioDeckB();
}


/* ##### audio playback ##### */
/* play and pause */
void MainWindow::playOrPauseDeckB()
{
    // get the current state of the audio player
    QMediaPlayer::PlaybackState stt = audioPlayerDeckB->playbackState();

    // if the state is pause then play and the button is clicked then play. If not in paused state pause the audio.
    if (stt == audioPlayerDeckB->PausedState || stt == audioPlayerDeckB->StoppedState) {
        playAudioDeckB();
    } else {
        pauseAudioDeckB();
    }
}

/* cue */
void MainWindow::cueDeckB()
{

    int indexBox1 = ui->outputDevice1BoxDeckB->currentIndex();
    int indexBox2 = ui->outputDevice2BoxDeckB->currentIndex();


    if(indexBox1 == indexBox2){

    }


    if(audioOutputDeckB->device() == audioDevicesDeckB->audioOutputs().at(indexBox1))
    {
        audioOutputDeckB->setDevice(audioDevicesDeckB->audioOutputs().at(indexBox2));
        ui->cueBtnDeckB->setIcon(QIcon(":/images/images/speaker_output.png"));
    }
    else
    {
        audioOutputDeckB->setDevice(audioDevicesDeckB->audioOutputs().at(indexBox1));
        ui->cueBtnDeckB->setIcon(QIcon(":/images/images/headphone_output.png"));
    }
}

/* stop */
void MainWindow::stopDeckB()
{
    audioPlayerDeckB->stop();
    ui->playPauseBtnDeckB->setIcon(QIcon(":/images/images/play.png"));
}

/* pitch */
void MainWindow::pitchSliderDeckB_valueChanged(int pitch)
{
    audioPlayerDeckB->setPlaybackRate(float(pitch)/1000);
    ui->pitchSliderDeckB->setSliderPosition(pitch);
    ui->speedLblDeckB->setText(QString::number( (float(pitch) / 1000 - 1.0) * 100) );
}

void MainWindow::pitchResetDeckB(){
    audioPlayerDeckB->setPlaybackRate(1.0);
    ui->pitchSliderDeckB->setSliderPosition(1000);
    ui->speedLblDeckB->setText(QString::number(0.0));
}

/* loudness button */
void MainWindow::muteUnmuteDeckB()
{
    // change the slider position and the volume if the loudness button is clicked
    if (ui->loudnessSliderDeckB->value() == 0.0) {
        audioOutputDeckB->setVolume(0.2);
        ui->loudnessSliderDeckB->setSliderPosition(20);
    } else {
        audioOutputDeckB->setVolume(0.0);
        ui->loudnessSliderDeckB->setSliderPosition(0);
    }
}

/* loudness slider */
void MainWindow::loudnessSliderDeckB_valueChanged(int value)
{
    // set the volume of the audio player. If the volume is 0 icon changes to mute and change the slide position and vice versa.
    audioOutputDeckB->setVolume(float(value)/100);

    if(value == 0) {
        ui->muteUnmuteBtnDeckB->setIcon(QIcon(":/images/images/mute.png"));
    } else {
        ui->muteUnmuteBtnDeckB->setIcon(QIcon(":/images/images/sound.png"));
    }

    ui->loudnessSliderDeckB->setSliderPosition(value);
}

/* set length of audio as label */
void MainWindow::setTimeLabelDeckB()
{
    // convert duration into HH:MM:SS format and set the label
    string timeLbl;
    long milli = audioPlayerDeckB->duration();
    long hr = milli / 3600000;
    milli = milli - 3600000 * hr;
    long min = milli / 60000;
    milli = milli - 60000 * min;
    long sec = milli / 1000;

    if (hr > 9) {
        timeLbl = to_string(hr) + ":";
    } else {
        timeLbl = "0" + to_string(hr) + ":";
    }
    if(min > 9){
        timeLbl += to_string(min) + ":";
    }else{
        timeLbl += "0" + to_string(min) + ":";
    }
    if(sec > 9){
        timeLbl += to_string(sec);
    }else{
        timeLbl += "0" + to_string(sec);
    }

    ui->absoluteTimeLabelDeckB->setText(QString::fromStdString(timeLbl));

}

/* set time progress */
void MainWindow::setTimeProgressDeckB()
{
    // convert duration into HH:MM:SS format and set the label
    string timeLbl;
    long milli = audioPlayerDeckB->position();
    long hr = milli / 3600000;
    milli = milli - 3600000 * hr;
    long min = milli / 60000;
    milli = milli - 60000 * min;
    long sec = milli / 1000;

    if (hr > 9) {
        timeLbl = to_string(hr) + ":";
    } else {
        timeLbl = "0" + to_string(hr) + ":";
    }
    if(min > 9){
        timeLbl += to_string(min) + ":";
    }else{
        timeLbl += "0" + to_string(min) + ":";
    }
    if(sec > 9){
        timeLbl += to_string(sec);
    }else{
        timeLbl += "0" + to_string(sec);
    }

    ui->timeProgressLabelDeckB->setText(QString::fromStdString(timeLbl));

    /* set position of turn table */
    int pos = 0;
    if(rpmDeckB == 45){
        pos = milli % 1333; // 60000 ms / 45 rpm = 1333,33 ms per rotation
    }
    else if(rpmDeckB == 33)
    {
        pos = milli % 1818; // 60000 ms / 33 rpm = 1818,18 ms per rotation
    }
    ui->turntableDeckB->setSliderPosition(pos);

}

void MainWindow::setTurntableProgressDeckB()
{
    /*
    long posTurntable = ui->turntableDeckB->sliderPosition();
    long posAudioPlayer = audioPlayerDeckB->position();
    long diff = posTurntable - (posAudioPlayer % 1333);
    audioPlayerDeckB->setPosition(posAudioPlayer - diff);
    */
}

/* play audio file */
void MainWindow::playAudioDeckB()
{
    // play the audio file and change the icon
    audioPlayerDeckB->play();
    ui->playPauseBtnDeckB->setIcon(QIcon(":/images/images/pause.png"));
}

/* pause audio file */
void MainWindow::pauseAudioDeckB()
{
    // pause the audio file and change the icon
    audioPlayerDeckB->pause();
    ui->playPauseBtnDeckB->setIcon(QIcon(":/images/images/play.png"));
}

void MainWindow::thirtyThreeRpmDeckB()
{
    ui->turntableDeckB->setRange(0,1818); // 60000 ms / 33 rpm = 1818,18 ms per rotation
    rpmDeckB = 33;
    ui->thirtyThreeRpmButtonDeckB->setStyleSheet("background-color: rgb(224, 195, 30)");
    ui->fourtyFiveRpmButtonDeckB->setStyleSheet("background-color: rgb(93, 91, 89)");
}

void MainWindow::fourtyFiveRpmDeckB()
{
    ui->turntableDeckB->setRange(0,1333); // 60000 ms / 45 rpm = 1333,33 ms per rotation
    rpmDeckB = 45;
    ui->fourtyFiveRpmButtonDeckB->setStyleSheet("background-color: rgb(224, 195, 30)");
    ui->thirtyThreeRpmButtonDeckB->setStyleSheet("background-color: rgb(93, 91, 89)");
}

void MainWindow::doublePitchRangeDeckB()
{
    if(pitchRangeDeckB == 8){
        ui->pitchSliderDeckB->setRange(840, 1160); // +/- 16% pitch
        pitchRangeDeckB = 16;
        ui->doublePitchDeckB->setStyleSheet("background-color: rgb(224, 195, 30)");
    }
    else if(pitchRangeDeckB == 16){
        /* clip pitch value when changing from 16% pitch to 8% pitch */
        if(ui->pitchSliderDeckB->value() < 920){
            pitchSliderDeckB_valueChanged(920);
        }
        if(ui->pitchSliderDeckB->value() > 1080){
            pitchSliderDeckB_valueChanged(1080);
        }

        ui->pitchSliderDeckB->setRange(920, 1080); // +/- 8% pitch
        pitchRangeDeckB = 8;
        ui->doublePitchDeckB->setStyleSheet("background-color: rgb(93, 91, 89)");
    }
}





/* update list of audio devices */
void MainWindow::updateAudioDevices()
{
    ui->outputDevice1BoxDeckA->clear();
    for (auto &deviceInfo1DeckA: audioDevicesDeckA->audioOutputs())
    {
        ui->outputDevice1BoxDeckA->addItem(deviceInfo1DeckA.description(), QVariant::fromValue(deviceInfo1DeckA));
    }

    ui->outputDevice2BoxDeckA->clear();
    for (auto &deviceInfo2DeckA: audioDevicesDeckA->audioOutputs())
    {
        ui->outputDevice2BoxDeckA->addItem(deviceInfo2DeckA.description(), QVariant::fromValue(deviceInfo2DeckA));
    }

    ui->outputDevice1BoxDeckB->clear();
    for (auto &deviceInfo1DeckB: audioDevicesDeckB->audioOutputs())
    {
        ui->outputDevice1BoxDeckB->addItem(deviceInfo1DeckB.description(), QVariant::fromValue(deviceInfo1DeckB));
    }

    ui->outputDevice2BoxDeckB->clear();
    for (auto &deviceInfo2DeckB: audioDevicesDeckB->audioOutputs())
    {
        ui->outputDevice2BoxDeckB->addItem(deviceInfo2DeckB.description(), QVariant::fromValue(deviceInfo2DeckB));
    }
}

void MainWindow::deviceChanged(int idx)
{
    if ((ui->outputDevice1BoxDeckA->count() == 0) || (ui->outputDevice2BoxDeckA->count() == 0) || (ui->outputDevice1BoxDeckB->count() == 0) || (ui->outputDevice2BoxDeckB->count() == 0))
    {
        return;
    }

    // device has changed
    deviceInfo1DeckA = ui->outputDevice1BoxDeckA->itemData(idx).value<QAudioDevice>();
    deviceInfo2DeckA = ui->outputDevice2BoxDeckA->itemData(idx).value<QAudioDevice>();
    deviceInfo1DeckB = ui->outputDevice1BoxDeckB->itemData(idx).value<QAudioDevice>();
    deviceInfo2DeckB = ui->outputDevice2BoxDeckB->itemData(idx).value<QAudioDevice>();
}





/* cross fader */
void MainWindow::crossFadeSlider_valueChanged(int value)
{
    audioOutputDeckA->setVolume(-1.0f * (float(value - 100)/100));
    audioOutputDeckB->setVolume(float(value)/100);

    //audioOutputDeckA->setVolume(-1.0f * (float(value - 100)/100) * audioOutputDeckA->volume());
    //audioOutputDeckB->setVolume(float(value)/100 * audioOutputDeckB->volume());


    ui->crossFadeSlider->setSliderPosition(value);
}
